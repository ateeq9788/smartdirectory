//
//  quoteDataView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 09/10/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class quoteDataView: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var quoteTableViewOutlet: UITableView!
    
    
    var dataFromServer:JSON = ["":""]
    
    var dataarray = [JSON]()
    
   var dataTransfer = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        quoteTableViewOutlet.delegate = self
        quoteTableViewOutlet.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
       self.apiCallFunc()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "quoteTableViewCell", for: indexPath) as! quoteTableViewCell
        
        
       
            
            
            cell.quoteLabelOutlet.text = self.dataarray[indexPath.row]["form_id"].stringValue
            
    
        
        
        cell.quoteLabelOutlet.layer.borderColor = UIColor.black.cgColor
        cell.quoteLabelOutlet.layer.borderWidth = 0.5
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.dataTransfer = [dataFromServer[indexPath.row]["form_content"]]
        self.performSegue(withIdentifier: "qoutdetail", sender: nil)
        
    }
    
    
    func apiCallFunc() {
        
        var josndata = ""
        
        Alamofire.request("https://acbd.com.au/iOS-api/getAllQuote.php", method: .post, parameters: nil, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()
            
            let json = try? JSON(data: response.data!)
            
            print("json data \(json!)")
            
            
            self.dataFromServer = (json!)
            
            
            
            for i in 0...self.dataFromServer.count
            {
                if self.dataFromServer[i]["form_content"].stringValue == "" || self.dataFromServer[i]["form_content"].stringValue == nil
                {
                    
                   
                    
                }
                else
                {
                    self.dataarray.append(self.dataFromServer[i])
                    
                }
            }
            
            
            print("data from server \(self.dataarray)")
           
            
            self.quoteTableViewOutlet.reloadData()
    }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "qoutdetail"
        {
            let vc = segue.destination as! MyQouteDetail
            vc.data = self.dataTransfer
        }
        
    }

    @IBAction func backButtonFunc(_ sender: UIButton) {
        
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
        
    }
    
    

   

} // end class

extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

