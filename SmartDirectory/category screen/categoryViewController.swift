//
//  categoryViewController.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 27/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class categoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    

    @IBOutlet weak var categoryTableViewOutlet: UITableView!
    var dataFromServer = [[String:Any]]()
    var catagoiresArray = [JSON]()
    var sortedCatagoiresArray = [String]()
    var catagoriesIDs = [String]()
    var sendingObject = [JSON]()
    
    var CatogeryData = [JSON]()
 
    var catogery = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    } // viewDidLoad
    
    override func viewWillAppear(_ animated: Bool) {
        
            self.getCatogery()
            self.categoryTableViewOutlet.reloadData()
           
            
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CatogeryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "catagoryTableCell", for: indexPath) as! catagoryTableCell
        
        cell.catagoryNameOutlet.text = self.CatogeryData[indexPath.row]["name"].stringValue
        cell.cellViewOutlet.layer.borderWidth = 0.5
        cell.cellViewOutlet.layer.borderColor = #colorLiteral(red: 0.376277864, green: 0.3685537577, blue: 0.3857637048, alpha: 1)
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "catogery"
        {
            let vc = segue.destination as! CatogerySubService
            vc.predata = self.sendingObject
        }
        
    }
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        self.sendingObject = [self.CatogeryData[indexPath.row]]
        
            self.performSegue(withIdentifier: "catogery", sender: nil)
        
    }

    func getCatogery(){
        

        
        
        Alamofire.request("https://acbd.com.au/iOS-api/getAllTerms.php", method: .post, parameters: nil, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            
            
            let json = try? JSON(data: response.data!)
            
            self.catagoiresArray = [json!]
            
            
//
            for index in 0...json!.count {
                
                if json![index]["taxonomy"].stringValue == "listing-category"
                {
                   
                self.CatogeryData.append(json![index])
                
                }
            }

            self.categoryTableViewOutlet.reloadData()
        }
        
    }
    
    
    
    
    @IBAction func backButtonFucn(_ sender: UIButton) {
        
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
        
    }
    

 

} // end of class
