//
//  aboutUsView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 16/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire

class aboutUsView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    } // end of viewDidLoad
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }

    @IBAction func backFunc(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
    }
    
   

} // end of aboutUsView Class
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleToFill,view:UIView) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                //                self.roundImage()
                self.image = image
                //ActivityIndicatorSingleton.StopActivity(myView: view)
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleToFill,view:UIView) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode,view:view)
    }
}
