//
//  CatogerySubService.swift
//  SmartDirectory
//
//  Created by ATEEQ  on 03/11/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class CatogerySubService: UIViewController , UIWebViewDelegate , UITableViewDelegate , UITableViewDataSource{

    @IBOutlet weak var webview: UIWebView!
    var catogery = ""
    var predata = [JSON]()
    var termid = ""
    var getserviceresponse : JSON = ["":""]
    
    @IBOutlet weak var tilelbl: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var postArray:JSON  = ["":""]
    
    let testing  = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tilelbl.text = self.predata[0]["name"].stringValue
        self.termid = self.predata[0]["term_id"].stringValue
        print("term id \(self.termid)")
        
     
        
        
       self.ServiceCall()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backbtn(_ sender: Any) {
        
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "categoryViewController") as! categoryViewController
        self.present(VC, animated: true, completion: nil)
    }
    
    func ServiceCall()
    {
        let parameter: Parameters = [
            "termID": "15"
        ]
        
        print("parameters \(parameter)")
        
        Alamofire.request("https://acbd.com.au/iOS-api/getSelectedPost.php?termID=\(self.termid)", method: .get, parameters: nil, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()

            let json = try? JSON(data: response.data!)

            print("json data \(json!)")

            self.getserviceresponse = json!

            
            
            
            
            self.tableview.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getserviceresponse.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell", for: indexPath) as! listingTableViewCell
        cell.nameOutlet.text = self.getserviceresponse[indexPath.row]["post_title"].stringValue
        cell.statusOutlet.text = postArray[indexPath.row]["ping_status"].stringValue
       
        cell.typeOutlet.text = self.tilelbl.text
        cell.imageOutlet.sd_setShowActivityIndicatorView(true)
        cell.imageOutlet.sd_setIndicatorStyle(.gray)
        cell.imageOutlet.sd_setImage(with: URL(string:self.getserviceresponse[indexPath.row]["attachment"].stringValue), placeholderImage: #imageLiteral(resourceName: "imageLoaderIcon") , options: [.highPriority], completed: nil)
        return cell

    }
    
}

