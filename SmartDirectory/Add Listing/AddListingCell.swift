//
//  AddListingCell.swift
//  SmartDirectory
//
//  Created by mac  on 07/11/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class AddListingCell: UITableViewCell {

    @IBOutlet weak var removebtn: UIButton!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var dayslbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
