//
//  ListingView.swift
//  SmartDirectory
//
//  Created by ATEEQ  on 04/11/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class ListingView: UIViewController , UIScrollViewDelegate {

    @IBOutlet weak var navigationview: UIView!
    @IBOutlet var scrollview: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.scrollview.delegate = self
        
        self.scrollview.frame = CGRect(x:0,y:self.navigationview.bounds.height + 100 ,width: self.view.frame.width ,height:self.scrollview.frame.height)
        
        
        self.scrollview.contentSize = CGSize(width: scrollview.contentSize.width, height:(self.scrollview.frame.height)+(self.scrollview.frame.height - self.view.frame.height)+150)
        
        self.view.addSubview(self.scrollview)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

  
}
