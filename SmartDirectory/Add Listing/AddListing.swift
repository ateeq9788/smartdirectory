
//
//  AddListing.swift
//  SmartDirectory
//
//  Created by ATEEQ  on 28/10/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddListing: UIViewController , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate , UIPickerViewDelegate ,UIPickerViewDataSource , UIImagePickerControllerDelegate , UINavigationControllerDelegate{

    @IBOutlet weak var instagramtxt: UITextField!
    @IBOutlet weak var youtubetxt: UITextField!
    @IBOutlet weak var googletxt: UITextField!
    @IBOutlet weak var facebooktxt: UITextField!
    @IBOutlet weak var linkedintxt: UITextField!
    @IBOutlet weak var twittertxt: UITextField!
    @IBOutlet weak var plusbtn: UIButton!
    @IBOutlet weak var timertale: UITableView!
    
    
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var chooseBuuton: UIButton!
    var imagePicker = UIImagePickerController()
    
    
    var dayspicker = UIPickerView()
    
    var timerpicker = UIPickerView()
    var evetimer =   UIPickerView()
    var catogerypicker = UIPickerView()
    
    var statpicker = UIPickerView()
    var pricepicker =   UIPickerView()
    
    @IBOutlet weak var profilepic: UIImageView!
    
  
    @IBOutlet weak var morningtimetxt: UITextField!
    
    @IBOutlet weak var pricerange: UITextField!
    @IBOutlet weak var catogerytxt: UITextField!
    @IBOutlet weak var statetxt: UITextField!
    @IBOutlet weak var eveningtimetxt: UITextField!
    @IBOutlet weak var daysinputtxt: UITextField!
    var days = ["Monday" , "Tuesday" , "Wednesday" , "Thursday" , "Friday" , "Saturday" , "Sunday"]
    
    var timer = ["24:00" , "23:30" , "23:00" , "22:30" , "22:00" , "21:30" , "21:00" , "20:00" , "19:30" , "19:00" , "18:30" , "18:00" , "17:30" , "17:00" , "16:30" , "16:00" , "15:30" , "15:00" , "14:30" , "14:00" , "13:30" , "13:00" , "12:30", "12:00", "11:30", "11:00", "11:30", "11:00", "10:30", "10:00", "9:30", "9:00", "8:30", "8:00", "7:30", "6:00", "5:30", "5:00", "4:30", "4:00", "3:30", "3:00", "2:30", "2:00", "1:30", "1:00"]
    
    
    var timeslot = ["09:00 - 17:00","09:00 - 17:00","09:00 - 17:00","09:00 - 17:00","09:00 - 17:00","09:00 - 17:00","09:00 - 17:00",]
    var catogery = [""]
    var state = [""]
    var pricerangearray = ["Not to say","$ - inexpensive","$$ - Moderate" , "$$$ - Pricey" , "$$$$ - Ultra Height"]
    
    
    
    
    
    @IBOutlet weak var signininfor: UILabel!
    @IBOutlet var uperview: UIView!
    @IBOutlet weak var signinview: UIView!
    
    @IBOutlet weak var innerview: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
        self.catorgeryData()
        self.stateData()
        
        
        self.uperview.isHidden = true
    
        self.uperview.frame = self.signinview.frame
        
        if UserDefaults.standard.bool(forKey: "isLogin") == true
        {
            self.signinview.isHidden = true
            self.uperview.isHidden = false
            self.innerview.addSubview(self.uperview)
            let user = UserDefaults.standard.string(forKey: "username")!
            
            self.signininfor.text = "as \(user), countinue"
            
            
        }
        
        
        
        
        
        
        self.dayspicker.tag = 1
        self.timerpicker.tag = 2
        self.evetimer.tag = 3
        self.catogerypicker.tag = 4
        self.statpicker.tag = 5
        self.pricepicker.tag = 6
        
        
        
        
        self.daysinputtxt.tag = 1
        self.morningtimetxt.tag = 2
        self.eveningtimetxt.tag = 3
        self.catogerytxt.tag = 4
        self.statetxt.tag = 5
        self.pricerange.tag = 6
        self.catogerytxt.delegate = self
        self.statetxt.delegate = self
        self.pricerange.delegate = self
        
        
        
        self.dayspicker.delegate = self
        self.timerpicker.delegate = self
        self.evetimer.delegate = self
        self.catogerypicker.delegate = self
        self.statpicker.delegate = self
        self.pricepicker.delegate = self
        
        
        
        
        // Do any additional setup after loading the view.
    }


    
    override func viewWillAppear(_ animated: Bool) {
        
        
         if UserDefaults.standard.bool(forKey: "isLogin") == true
         {
                    var pic = UserDefaults.standard.string(forKey: "profilepic")
            
                    if UserDefaults.standard.string(forKey: "profilepic")!.isEmpty
                    {
            
                    }
                    else
                    {
                        pic = UserDefaults.standard.string(forKey: "profilepic")!
                        self.profilepic.downloadedFrom(link:pic!, view: self.view)
                    }
            
            
            
                    self.profilepic?.layer.borderWidth = 2
                    self.profilepic?.layer.masksToBounds = false
            
                    profilepic?.layer.cornerRadius = (profilepic?.frame.height)!/2
                    profilepic?.clipsToBounds = true
            
            
            
            let facebook = UserDefaults.standard.string(forKey: "facebook")
            
            let google = UserDefaults.standard.string(forKey: "google")
            let linkedin = UserDefaults.standard.string(forKey: "linkedin")
            let instagram = UserDefaults.standard.string(forKey: "instagram")
            let twitter = UserDefaults.standard.string(forKey: "twitter")
           
            self.facebooktxt.text = facebook
            self.googletxt.text = google
            self.linkedintxt.text = linkedin
            self.instagramtxt.text = instagram
            self.twittertxt.text = twitter
        }
        

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.days.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell") as! AddListingCell
        cell.dayslbl.text =  self.days[indexPath.row]
        cell.timelbl.text = self.timeslot[indexPath.row]
        return cell
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        
        if (pickerView.tag == 1)
        {
            return days.count
            
        }
        else if (pickerView.tag == 2)
        {
            
            return timer.count
        }
        else if (pickerView.tag == 3)
        {
            
            return timer.count
        }
            
        else if (pickerView.tag == 4)
        {
            
            return catogery.count
        }
            
        else if (pickerView.tag == 5)
        {
            
            return state.count
        }
            
        else if (pickerView.tag == 6)
        {
            
            return pricerangearray.count
        }
        
        return 0
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if (pickerView.tag == 1)
        {
            return days[row]
            
        }
        else if (pickerView.tag == 2)
        {
            
            return timer[row]
        }
            
        else if (pickerView.tag == 3)
        {
            
            return timer[row]
        }
            
        else if (pickerView.tag == 4)
        {
            
            return catogery[row]
        }
            
        else if (pickerView.tag == 5)
        {
            
            return state[row]
        }
            
        else if (pickerView.tag == 6)
        {
            
            return pricerangearray[row]
        }
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if (pickerView.tag == 1)
        {
            self.daysinputtxt.text = days[row]
        }
            
        else if (pickerView.tag == 2)
        {
            self.morningtimetxt.text = timer[row]
            
        }
            
        else if (pickerView.tag == 3)
        {
            self.eveningtimetxt.text = timer[row]
            
        }
            
        else if (pickerView.tag == 4)
        {
            self.catogerytxt.text = catogery[row]
            
        }
        else if (pickerView.tag == 5)
        {
            self.statetxt.text = state[row]
            
        }
        else if (pickerView.tag == 6)
        {
            self.pricerange.text = pricerangearray[row]
            
        }
        self.view.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1
        {
            textField.inputView = dayspicker
        }
        else if textField.tag == 2
        {
            textField.inputView = timerpicker
        }
            
        else if textField.tag == 3
        {
            textField.inputView = evetimer
        }
            
        else if textField.tag == 4
        {
            textField.inputView = catogerypicker
        }
        else if textField.tag == 5
        {
            textField.inputView = statpicker
        }
        else if textField.tag == 6
        {
            textField.inputView = pricepicker
        }
        
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        
        if (self.daysinputtxt.text?.isEmpty)! == false && (self.morningtimetxt.text?.isEmpty)! == false && (self.eveningtimetxt.text?.isEmpty ) == false
        {
            self.plusbtn.isEnabled = true
        }
        
        return true
        
    }
    
    
    func catorgeryData(){
        
        Alamofire.request("https://acbd.com.au/iOS-api/getDashboardDetails.php", method: .post, parameters: nil, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()
            
            let json = try? JSON(data: response.data!)
            
            print("json data \(json!)")
            let newdata = json![0]["term"]
            
            print("term data \(newdata)")
        
            
            for i in 0...newdata.count {
                self.catogery.append(newdata[i]["name"].stringValue)
            }
            
            print("catogery array \(self.catogery)")
    }
    
    }
    
    //
    
    
    func stateData(){
        
        Alamofire.request("https://acbd.com.au/iOS-api/getAllLocations.php", method: .post, parameters: nil, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()
            
            let json = try? JSON(data: response.data!)
            
            print("json data \(json!)")
            
            
            
            for i in 0...json!.count {
                self.state.append(json![i]["name"].stringValue)
            }
            
            print("state value \(self.state)")
        }
        
    }
    
    
    @IBAction func imagebrowsebtn(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)
        {
            print("Button capture")
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        if imageView.image == nil{
        imageView.image = chosenImage
        }
        else if imageView.image != nil
        {
            image2.image = chosenImage
        }
        else if image2.image != nil
        {
            image3.image = chosenImage
        }
        dismiss(animated: true, completion: nil)
    }
        
 
    
    @IBAction func timeraddbtn(_ sender: Any) {
        print("pressed"); self.days.append(self.daysinputtxt.text!)
        
        self.timeslot.append("\(self.morningtimetxt.text!) - \(self.eveningtimetxt.text!)")
        
        print("days value \(self.days) and timer is \(self.timeslot)")
        self.timertale.reloadData()
        self.daysinputtxt.text = ""
        self.morningtimetxt.text = ""
        self.eveningtimetxt.text = ""
    }
    
    
    @IBAction func signinbtn(_ sender: Any) {
        
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "signInView") as! signInView
        self.present(VC, animated: true, completion: nil)
        
    }
    
    @IBAction func backbtn(_ sender: Any) {
            let story = UIStoryboard(name: "Main", bundle : nil)
            let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
            self.present(VC, animated: true, completion: nil)
           
    }
    
 
}
