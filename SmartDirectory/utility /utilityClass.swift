//
//  utilityClass.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 17/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
struct sliderDetail : Codable{
    var id : Int
    var name : String
    var image : String
    var job : String
    var phone : String
    var email : String
    var describe : String
    var facebook : String
    var twitter : String
    var google : String
    var linkeding : String
    var attachment : String
    
    init(_ dictionary:[String:Any]) {
        self.id = dictionary["id"] as? Int ?? 0
        self.name = dictionary["name"] as? String ?? ""
        self.image = dictionary["image"] as? String ?? ""
        self.job = dictionary["job"] as? String ?? ""
        self.phone = dictionary["phone"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
        self.describe = dictionary["describe"] as? String ?? ""
        self.facebook = dictionary["facebook"] as? String ?? ""
        self.twitter = dictionary["twitter"] as? String ?? ""
        self.google = dictionary["google"] as? String ?? ""
        self.linkeding = dictionary["linkeding"] as? String ?? ""
        self.attachment = dictionary["attachment"] as? String ?? ""
    }
    
}



struct postDetail {
    var id : Int
    var post_Title : String
    var comment_status : String
    var otherDetails : String
    var otherDetailsField : String
    var attachment : String
    
    
    init(_ dictionary:[String:Any]) {
        self.id = dictionary["id"] as? Int ?? 0
        self.post_Title = dictionary["post_title"] as? String ?? ""
        self.comment_status = dictionary["comment_status"] as? String ?? ""
        self.otherDetails = dictionary["otherDetails"] as? String ?? ""
        self.otherDetailsField = dictionary["otherDetailsField"] as? String ?? ""
        self.attachment = dictionary["attachment"] as? String ?? ""
    }
}


struct userDetail {
    var firstName : String = ""
    var lastName : String = ""
    var email : String = ""
    var phone : String = ""
    var address : String = ""
    var about : String = ""
    var facebook : String = ""
    var twitter : String = ""
    var linkedin : String = ""
    var google : String = ""
    var instgram : String = ""
    var pinterest : String = ""
    var pic : String = ""
    
    init(_ dictionary: [String:Any]) {
        var userdeatils = [[String:Any]]()
        var arr = [String:Any]()
        
        self.email = dictionary["user_email"] as? String ?? ""
        for data in dictionary {
            if data.key == "userDeatils" {
                userdeatils = data.value as! [[String:Any]]
            }
        }
        for data in userdeatils {
            for (key,value) in data{
                if key == "meta_key" && value as! String == "phone"  {
                    arr = data
                    self.phone = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "address"{
                    arr = data
                    self.address = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "facebook"{
                    arr = data
                    self.facebook = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "google"{
                    arr = data
                    self.google = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "linkedin"{
                    arr = data
                    self.linkedin = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "twitter"{
                    arr = data
                    self.twitter = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "pinterest"{
                    arr = data
                    self.pinterest = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "listingpro_author_img_url"{
                    arr = data
                    self.pic = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "first_name"{
                    arr = data
                    self.firstName = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "last_name"{
                    arr = data
                    self.lastName = arr["meta_value"] as! String
                }
                else if key == "meta_key" && value as! String == "description"{
                    arr = data
                    self.about = arr["meta_value"] as! String
                }
                
            }
            
        }
    }
}
class utilityClass {

}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

