//
//  termAndConditionView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 16/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class termAndConditionView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func backFunc(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
    }
   

} // end of termAndConditionView class
