//
//  ContactUSView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 17/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import MapKit

class customMarker : NSObject,MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    
    init(location: CLLocationCoordinate2D, title : String){
        self.coordinate = location
        self.title = title
        
    }
}

class ContactUSView: UIViewController,UITextFieldDelegate,UITextViewDelegate,MKMapViewDelegate , UIScrollViewDelegate {

    //153.393516  //-27.927092
    @IBOutlet weak var nameTextFieldOutlet: UITextField!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    @IBOutlet weak var subjectTextFieldOutlet: UITextField!
    @IBOutlet weak var messageTextViewOutlet: UITextView!
    @IBOutlet weak var mapViewOutlet: UIView!
    @IBOutlet weak var backGroundViewOutlet: UIView!
    @IBOutlet weak var mapkitViewOutlet: MKMapView!
    
    @IBOutlet var scrollview: UIScrollView!
    
    @IBOutlet weak var navigationview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.scrollview.delegate = self
        
        
        self.scrollview.frame = CGRect(x:0,y:self.navigationview.frame.size.height + 5,width: self.view.frame.width ,height:self.scrollview.frame.height)
        
        
        self.scrollview.contentSize = CGSize(width: scrollview.contentSize.width - 5, height:(self.scrollview.frame.height)+(self.scrollview.frame.height - self.view.frame.height) + 50)
        
        self.view.addSubview(self.scrollview)
        
        
        
        
        let location = CLLocationCoordinate2D(latitude: -27.927092 , longitude: 153.393516)
        let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.010, longitudeDelta: 0.010))
        self.mapkitViewOutlet.setRegion(region, animated: true)
       // self.mapkitViewOutlet.setCenter(location, animated: true)
        
        
        let pin = customMarker(location: location, title: "check")
        self.mapkitViewOutlet.addAnnotation(pin)
        self.mapkitViewOutlet.delegate = self
        
        nameTextFieldOutlet.layer.borderWidth = 2
        nameTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        nameTextFieldOutlet.layer.cornerRadius = 6
        nameTextFieldOutlet.delegate = self
        nameTextFieldOutlet.setLeftPaddingPoints(15)
        
        emailTextFieldOutlet.layer.borderWidth = 2
        emailTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        emailTextFieldOutlet.layer.cornerRadius = 6
        emailTextFieldOutlet.delegate = self
        emailTextFieldOutlet.setLeftPaddingPoints(15)
        
        subjectTextFieldOutlet.layer.borderWidth = 2
        subjectTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        subjectTextFieldOutlet.layer.cornerRadius = 6
        subjectTextFieldOutlet.delegate = self
        subjectTextFieldOutlet.setLeftPaddingPoints(15)
        
        messageTextViewOutlet.layer.borderWidth = 2
        messageTextViewOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        messageTextViewOutlet.layer.cornerRadius = 6
        messageTextViewOutlet.delegate = self
        messageTextViewOutlet.text = "Message :"
        messageTextViewOutlet.textColor = UIColor.lightGray
        messageTextViewOutlet.contentInset = UIEdgeInsets(top: 3,left: 15,bottom: 5,right: 5)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(signInView.tapGestureToBackGrounViewFunc))
        backGroundViewOutlet.isUserInteractionEnabled = true
        backGroundViewOutlet.addGestureRecognizer(tapGesture)
        
        
   
    } // end of viewDidLoad
    
    @objc func tapGestureToBackGrounViewFunc(sender:UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if messageTextViewOutlet.textColor == UIColor.lightGray {
            messageTextViewOutlet.text = nil
            messageTextViewOutlet.textColor =  #colorLiteral(red: 0.5176073909, green: 0.5176702738, blue: 0.517578423, alpha: 1)
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if messageTextViewOutlet.text.isEmpty {
            messageTextViewOutlet.text = "Message :"
            messageTextViewOutlet.textColor = UIColor.lightGray
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "custom")
        annotationView.canShowCallout = true
        annotationView.image = #imageLiteral(resourceName: "contactMapMarker")
        return annotationView
        
    }
    
    @IBAction func backFunc(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
    }

    @IBAction func submitFunc(_ sender: UIButton) {
        let alert = UIAlertController(title: "Alert", message: "Submit Successfull", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

} // end of ContactUSView Class
