//
//  splashViewController.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 16/10/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class splashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        //adding observer
        NotificationCenter.default.addObserver(self, selector: #selector(splashViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let checkNet = checkInternet()
        if checkNet == true {
            let story = UIStoryboard(name: "Main", bundle : nil)
            let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
            self.present(VC, animated: true, completion: nil)
            }
        else{
            
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo
        print(userInfo?.description as Any)
    }
    
    // function to check internet connectivity
    func checkInternet()-> Bool {
        let networkStatus = Reach().connectionStatus()
        switch networkStatus {
        case .unknown, .offline:
            showAlert(message: "No Internet Connectivity!")
            print("Not connected")
            return false
        case .online(.wwan):
            print("Connected via WWAN")
            return true
        case .online(.wiFi):
            print("Connected via WiFi")
            return true
        }
    } // end of checkInternet
    
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: {action in
            let checkNet = self.checkInternet()
            if checkNet == true {
                let story = UIStoryboard(name: "Main", bundle : nil)
                let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
                self.present(VC, animated: true, completion: nil)
            }
            else{
                self.showAlert(message: "No Internet Connectivity!")
            }
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    

   
    

   
} // end of class
