//
//  listingTableViewCell.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 25/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class listingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageOutlet: UIImageView!
    @IBOutlet weak var nameOutlet: UILabel!
    @IBOutlet weak var typeOutlet: UILabel!
    @IBOutlet weak var tagOutlet: UILabel!
    @IBOutlet weak var stateOutlet: UILabel!
    @IBOutlet weak var suburbOutlet: UILabel!
    @IBOutlet weak var addressOutlet: UILabel!
    @IBOutlet weak var statusOutlet: UILabel!
  
    
    //Table What
    @IBOutlet weak var whatLabelOutlet: UILabel!
    
    
    //Table Where
    @IBOutlet weak var whereLabelOutlet: UILabel!
    
    
}
