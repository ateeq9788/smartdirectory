//
//  sliderCollectionViewCell.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 02/10/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class sliderCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var sliderImageOutlet: UIImageView!
    @IBOutlet weak var sliderNameOutlet: UILabel!
    @IBOutlet weak var sliderDescriptionOutlet: UILabel!
    
    
}
