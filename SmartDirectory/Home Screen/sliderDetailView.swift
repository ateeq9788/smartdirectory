//
//  sliderDetailView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 03/10/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class sliderDetailView: UIViewController {

    @IBOutlet weak var titleNameOutlet: UILabel!
    @IBOutlet weak var imageOutlet: UIImageView!
    @IBOutlet weak var NameOutlet: UILabel!
    @IBOutlet weak var jobOutlet: UILabel!
    @IBOutlet weak var descrptionOutlet: UITextView!
    @IBOutlet weak var emailOutlet: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let data = UserDefaults.standard.data(forKey: "currentSliderClick"),
            let detail = try? JSONDecoder().decode(sliderDetail.self, from: data) {
            titleNameOutlet.text = detail.name
            imageOutlet.sd_setShowActivityIndicatorView(true)
            imageOutlet.sd_setIndicatorStyle(.gray)
            imageOutlet.sd_setImage(with: URL(string:detail.attachment), placeholderImage: #imageLiteral(resourceName: "imageLoaderIcon") , options: [.highPriority], completed: nil)
            NameOutlet.text = detail.name
            jobOutlet.text = detail.job
            descrptionOutlet.text = detail.describe
            emailOutlet.text = detail.email
        }

    }

    @IBAction func backButtonFucn(_ sender: UIButton) {
        
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
        
    }

 

} // end of class
