//
//  homeScreenView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 18/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON



var objectSideMenuView = sideMenuView()
class homeScreenView: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {

    var dataFromServer = [JSON]()
    
    @IBOutlet weak var whatViewOutlet: UIView!
    @IBOutlet weak var whereViewOutlet: UIView!
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBOutlet weak var requestButtonOutlet: UIButton!
    @IBOutlet weak var nameTextFieldOutlet: UITextField!
    @IBOutlet weak var organisationTextFieldOutlet: UITextField!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    @IBOutlet weak var phoneTextFieldOutlet: UITextField!
    @IBOutlet weak var stateTextFieldOutlet: UITextField!
    @IBOutlet weak var listingTableViewOutlet: UITableView!
    @IBOutlet weak var listingBackOutlet: UIView!
    @IBOutlet weak var whatTableViewOutlet: UITableView!
    @IBOutlet weak var whereTableViewOutlet: UITableView!
    @IBOutlet weak var whatButtonOutlet: UIButton!
    @IBOutlet weak var whereButtonOutlet: UIButton!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    
    var story = UIStoryboard(name: "Main", bundle : nil)
    var VC = UIViewController()
    
    var dataFromServerWhat = [[String:Any]]()
    var dataFromServerWhere = [[String:Any]]()
    var dataFromWhatTable = [[String:Any]]()
    var dataFromSliderTable = [[String:Any]]()
    var dataFromPostTable = [[String:Any]]()
    var whatArray = [String]()
    var whereArray = [String]()
    var whatSortedArray = [String]()
    var whereSortedArray = [String]()
    var sliderArray : [sliderDetail] = []
    var postArray : [postDetail] = []
    var scrollingTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
      
        
        
        whereTableViewOutlet.tag = 2
        pageController.currentPage = 0
        objectSideMenuView = self.storyboard?.instantiateViewController(withIdentifier: "sideMenuView") as! sideMenuView

        whatViewOutlet.layer.borderWidth = 0.5
        whatViewOutlet.layer.borderColor = UIColor.black.cgColor
        
        whereViewOutlet.layer.borderWidth = 0.5
        whereViewOutlet.layer.borderColor = UIColor.black.cgColor
        
        searchButtonOutlet.layer.cornerRadius = 10
        requestButtonOutlet.layer.cornerRadius = 10
        
        nameTextFieldOutlet.delegate = self
        organisationTextFieldOutlet.delegate = self
        emailTextFieldOutlet.delegate = self
        phoneTextFieldOutlet.delegate = self
        stateTextFieldOutlet.delegate = self
        
        listingTableViewOutlet.delegate = self
        listingTableViewOutlet.dataSource = self
        whatTableViewOutlet.delegate = self
        whatTableViewOutlet.dataSource = self
        whatTableViewOutlet.isHidden = true
        whereTableViewOutlet.delegate = self
        whereTableViewOutlet.dataSource = self
        whereTableViewOutlet.isHidden = true
        
        sliderCollectionView.dataSource = self
        sliderCollectionView.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(signInView.tapGestureToBackGrounViewFunc))
        listingBackOutlet.isUserInteractionEnabled = true
        listingBackOutlet.addGestureRecognizer(tapGesture)
        
        refresh()
        
    } // end of viewDidLoad
    
    override func viewWillAppear(_ animated: Bool) {
       // DispatchQueue.global(qos: .background).async {
            self.getWhatCatagoriesFunc()
            self.getWhereLocationFunc()
        //}
        
        //DispatchQueue.global(qos: .background).async {
            self.getWhereLocationFunc()
        //}
    }
    
    @objc func tapGestureToBackGrounViewFunc(sender:UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nameTextFieldOutlet.resignFirstResponder()
        organisationTextFieldOutlet.resignFirstResponder()
        emailTextFieldOutlet.resignFirstResponder()
        phoneTextFieldOutlet.resignFirstResponder()
        stateTextFieldOutlet.resignFirstResponder()
        return true
    }
    
    @IBAction func whatDropDownFunc(_ sender: UIButton) {
        whatTableViewOutlet.isHidden = !whatTableViewOutlet.isHidden
    }
    
    @IBAction func whereDropDownFunc(_ sender: UIButton) {
        whereTableViewOutlet.isHidden = !whereTableViewOutlet.isHidden
    }
    
    
    func showMenuFunc() {
        AppDelegate.menuBool = false
        UIView.animate(withDuration: 0.3){ ()->Void in
            objectSideMenuView.view.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            objectSideMenuView.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.addChildViewController(objectSideMenuView)
            self.view.addSubview(objectSideMenuView.view)
        }
    }
    
    func hideMenuFunc() {
        AppDelegate.menuBool = true
        UIView.animate(withDuration: 0.3){ ()->Void in
            objectSideMenuView.view.frame = CGRect(x:-UIScreen.main.bounds.size.width, y: 0, width:UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            objectSideMenuView.view.removeFromSuperview()
        }
    }
    
    
    
    @IBAction func youtubelinkbtn(_ sender: Any) {
        
        
        if let url = URL(string: "https://www.youtube.com"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
        
    }
    
    
    
    
    func sendmail(){
        
        let subject = "\(self.nameTextFieldOutlet.text!) \n \(self.organisationTextFieldOutlet.text!)"
        let email = self.emailTextFieldOutlet.text!
        let message = self.stateTextFieldOutlet.text!
        
        let parameters: Parameters = [
            "subject": subject,
            "from_address": email,
            "message" : message
            
            
        ]
        
        
        Alamofire.request("https://acbd.com.au/iOS-api/send_email.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()
            
            let json = try? JSON(data: response.data!)
            
            print("json data \(json!)")
            let message = json!["msg"].stringValue
            
            
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
    }
 }
    
    
    @IBAction func sideMenuFunction(_ sender: UIButton) {
        if AppDelegate.menuBool{
            showMenuFunc()
        }
        else {
            hideMenuFunc()
        }
    }
    
    func getWhatCatagoriesFunc() {
        
        guard let url = URL(string: "https://acbd.com.au/iOS-api/getDashboardDetails.php") else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                //here dataResponse received from a network request
                self.dataFromServerWhat = try JSONSerialization.jsonObject(with:
                    dataResponse, options: []) as? NSArray as! [[String : Any]]
                
                self.filterFromWhatFunc()
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    } //end
    
    
    func filterFromWhatFunc() {
        for data in dataFromServerWhat {
            for (key,value) in data {
                if key == "term"{
                    dataFromWhatTable = value as! [[String:Any]]
                }
                if key == "slider"{
                    dataFromSliderTable = value as! [[String:Any]]
                }
                if key == "post" {
                    dataFromPostTable = value as! [[String:Any]]
                }
            }
        }
        
        for data in dataFromWhatTable {
            for (key,value) in data {
                if key == "name" {
                    whatArray.append(value as! String)
                }
            }
        }
        
        for data in dataFromSliderTable {
            let new = sliderDetail(data)
            sliderArray.append(new)
        }
        
        for data in dataFromPostTable {
            let new = postDetail(data)
            postArray.append(new)
            
        }
        
        
        whatSortedArray = whatArray.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        
       // DispatchQueue.main.async {
            
        self.whatArray = self.whatSortedArray
            
            self.whatTableViewOutlet.reloadData()
            self.listingTableViewOutlet.reloadData()
            self.sliderCollectionView.reloadData()
            
        //}
    } // end
    
    func getWhereLocationFunc() {
        guard let url = URL(string: "https://acbd.com.au/iOS-api/getAllLocations.php") else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                //here dataResponse received from a network request
                self.dataFromServerWhere = try JSONSerialization.jsonObject(with:
                    dataResponse, options: []) as? NSArray as! [[String : Any]]
                
                self.filterFromWhereFunc()
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
    
    func filterFromWhereFunc() {
        for data in dataFromServerWhere {
            for (key,value) in data {
                if key == "name" {
                   
                    whereArray.append(value as! String)
                }
            }
            
            
            
        }
        for value in whereArray
        {
            if !whereSortedArray.contains(value)
            {
        whereSortedArray = whereArray.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            }
        }
        
        
        DispatchQueue.main.async {
        
            self.whereArray = self.whereSortedArray
            self.whereTableViewOutlet.reloadData()
        }
        
    } // end

    @IBAction func requestQuoteFunc(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "requestQuoteView") as! requestQuoteView
        self.present(VC, animated: true, completion: nil)
    }
    @IBAction func submitButtonFunc(_ sender: UIButton) {
        
        
        if (nameTextFieldOutlet.text?.isEmpty)! && (organisationTextFieldOutlet.text?.isEmpty)! &&
            (stateTextFieldOutlet.text?.isEmpty)! && (emailTextFieldOutlet!.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Alert", message: "Fill the required fields", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        else{
          
            
            self.sendmail()
            
        }
    }
    
    
    
    func submitservice()
    {
        guard let url = URL(string: "https://acbd.com.au/iOS-api/getDashboardDetails.php") else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                
                self.dataFromServerWhat = try JSONSerialization.jsonObject(with:
                    dataResponse, options: []) as? NSArray as! [[String : Any]]
                
                self.filterFromWhatFunc()
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }

} // end of homeScreenView class

//for tableView
extension homeScreenView {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return whatSortedArray.count
        }
        else if tableView.tag == 2{
            return whereSortedArray.count
        }
        else {
            return postArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "whatCell", for: indexPath) as! listingTableViewCell
            cell.whatLabelOutlet.text = whatArray[indexPath.row]
            return cell
        }
            
        else if tableView.tag == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "whereCell", for: indexPath) as! listingTableViewCell
            cell.whereLabelOutlet.text = whereArray[indexPath.row]
            return cell
        }
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell", for: indexPath) as! listingTableViewCell
            cell.nameOutlet.text = postArray[indexPath.row].post_Title
            cell.statusOutlet.text = postArray[indexPath.row].comment_status
            cell.imageOutlet.sd_setShowActivityIndicatorView(true)
            cell.imageOutlet.sd_setIndicatorStyle(.gray)
            cell.imageOutlet.sd_setImage(with: URL(string:postArray[indexPath.row].attachment), placeholderImage: #imageLiteral(resourceName: "imageLoaderIcon") , options: [.highPriority], completed: nil)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            whatTableViewOutlet.isHidden = true
            whatButtonOutlet.setTitle(whatArray[indexPath.row], for: .normal)
        }
        else if tableView.tag == 2{
            whereTableViewOutlet.isHidden = true
            whereButtonOutlet.setTitle(whereArray[indexPath.row], for: .normal)
        }
    }
    

} // end tableView extension





 // for collectionView
extension homeScreenView: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageController.numberOfPages = sliderArray.count
        pageController.currentPage = 0
        return sliderArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "slider", for: indexPath) as! sliderCollectionViewCell
        cell.sliderNameOutlet.text = sliderArray[indexPath.row].name
        cell.sliderDescriptionOutlet.text = sliderArray[indexPath.row].job
        cell.sliderImageOutlet.sd_setShowActivityIndicatorView(true)
        cell.sliderImageOutlet.sd_setIndicatorStyle(.gray)
        cell.sliderImageOutlet.sd_setImage(with: URL(string:sliderArray[indexPath.row].attachment), placeholderImage: #imageLiteral(resourceName: "imageLoaderIcon") , options: [.highPriority], completed: nil)
        return cell
        
    }
    
    func refresh() {
        scrollingTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startTimerFunc(theTimer:)), userInfo: nil, repeats: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let new = sliderArray[indexPath.row]
        if let encoded = try? JSONEncoder().encode(new) {
            UserDefaults.standard.set(encoded, forKey: "currentSliderClick") }
        VC = story.instantiateViewController(withIdentifier: "sliderDetailView") as! sliderDetailView
        self.present(VC, animated: true, completion: nil)

    }
    
    @objc func startTimerFunc(theTimer: Timer) {
        let collectionBounds = self.sliderCollectionView.bounds
        let contentOffset = CGFloat(floor(self.sliderCollectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset,collection: sliderCollectionView)
        pageController.currentPage = pageController.currentPage + 1
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat, collection:UICollectionView) {
        let frame: CGRect = CGRect(x : contentOffset ,y : collection.contentOffset.y ,width : collection.frame.width,height : collection.frame.height)
        collection.scrollRectToVisible(frame, animated: true)
    }
    
   /* @objc func scrollToNextCell(){
        
        //get cell size
        let cellSize = view.frame.size
        
        //get current content Offset of the Collection view
        let contentOffset = sliderCollectionView.contentOffset
        pageController.numberOfPages = sliderArray.count
        
        if sliderCollectionView.contentSize.width <= sliderCollectionView.contentOffset.x + cellSize.width
        {
            pageController.currentPage = 0
            let r = CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            sliderCollectionView.scrollRectToVisible(r, animated: true)
            
        } else {
            pageController.currentPage = pageController.currentPage + 1
            
            let r = CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            sliderCollectionView.scrollRectToVisible(r, animated: true);
        }
        
    }
    
    func startTimer() {
        _ =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
    }*/
}
