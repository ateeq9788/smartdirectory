//
//  listingViewController.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 25/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class listingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var titleOutlet: UILabel!
    @IBOutlet weak var listingTableViewOutlet: UITableView!
    @IBOutlet weak var errorLabelOutlet: UILabel!
    
    var dataFromServer = [[String:Any]]()
    var postArray : [postDetail] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        listingTableViewOutlet.delegate = self
        listingTableViewOutlet.dataSource = self
        
    } // end of viewDidload
    
    override func viewWillAppear(_ animated: Bool) {
        let name = UserDefaults.standard.string(forKey: "selectedTermName")
        titleOutlet.text = name
        let id = UserDefaults.standard.string(forKey: "selectedTermID")
        let parameter = "termID=\(String(describing: id!))"
        print(parameter)
        
        DispatchQueue.global(qos: .background).async {
            self.apiCallFunc(postString: parameter)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "catagoryListingCell", for: indexPath) as! catagoryListingCell
        cell.nameOutlet.text = postArray[indexPath.row].post_Title
        cell.statusOutlet.text = postArray[indexPath.row].comment_status
        cell.imageOutlet.sd_setShowActivityIndicatorView(true)
        cell.imageOutlet.sd_setIndicatorStyle(.gray)
        cell.imageOutlet.sd_setImage(with: URL(string:postArray[indexPath.row].attachment), placeholderImage: #imageLiteral(resourceName: "imageLoaderIcon") , options: [.highPriority], completed: nil)
        return cell
    }
    
    
    func apiCallFunc(postString : String) {
        
        let url = URL(string: "https://acbd.com.au/iOS-api/getSelectedPost.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Check for error
            if error != nil {
                print("error=\(String(describing: error))")
                //  self.databaseError = error
                return
            }
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            do {
                //saving json data from response string url as nsdictionary
                self.dataFromServer = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray as! [[String : Any]]
                print(self.dataFromServer)
                self.filterData()
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        task.resume()
    } // end

    func filterData(){
        if dataFromServer.isEmpty == false {
            for data in dataFromServer {
                let new = postDetail(data)
                postArray.append(new)
            }
            DispatchQueue.main.async {
                self.listingTableViewOutlet.reloadData()
            }
        }
        else{
            DispatchQueue.main.async {
                self.errorLabelOutlet.isHidden = false
                self.listingTableViewOutlet.isHidden = true
            }
            
        }
        
    }// end
    
    @IBAction func backButtonFucn(_ sender: UIButton) {
        
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "categoryViewController") as! categoryViewController
        self.present(VC, animated: true, completion: nil)


//        AppDelegate.menuBool = true
//        VC = story.instantiateViewController(withIdentifier: "categoryViewController") as! categoryViewController
//        self.present(VC, animated: true, completion: nil)


    }
    

} // end of class
