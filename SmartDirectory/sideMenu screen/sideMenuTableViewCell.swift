//
//  sideMenuTableViewCell.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 16/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class sideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var sideImageOutlet: UIImageView!
    @IBOutlet weak var sideLabelOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    

} // end of sideMenuTableViewCell class
