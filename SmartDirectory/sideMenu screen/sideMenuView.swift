//
//  sideMenuView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 16/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

var objectHome = homeScreenView()
class sideMenuView: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var useremail: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var userinfoview: UIView!
    @IBOutlet weak var sideTableViewOutlet: UITableView!
    @IBOutlet weak var backViewOutlet: UIView!
    
    struct section {
        
        var sectionName : String!
        var sectionOjbect : [String]!
        var sectionImages : [UIImage]!
    }
    
    var story = UIStoryboard(name: "Main", bundle : nil)
    var VC = UIViewController()
    var images = [#imageLiteral(resourceName: "sideHome"),#imageLiteral(resourceName: "sideRequest"),#imageLiteral(resourceName: "sideAboutUs"),#imageLiteral(resourceName: "sideAboutUs"),#imageLiteral(resourceName: "sideAddListining"),#imageLiteral(resourceName: "sideAddListining")]
    var objectSection = [section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userinfoview.isHidden = true
        
        sideTableViewOutlet.delegate = self
        sideTableViewOutlet.dataSource = self
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        sideTableViewOutlet.tableHeaderView = UIView(frame: frame)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(sideMenuView.tapFunction))
        backViewOutlet.isUserInteractionEnabled = true
        backViewOutlet.addGestureRecognizer(tap)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if UserDefaults.standard.bool(forKey: "isLogin") == true {
            objectSection = [section(sectionName: "", sectionOjbect: ["Home","Request a Quote","Categories","About US","Add Listning","Afflitate Area"], sectionImages: [#imageLiteral(resourceName: "sideHome"),#imageLiteral(resourceName: "sideRequest"),#imageLiteral(resourceName: "sideAboutUs"),#imageLiteral(resourceName: "sideAboutUs"),#imageLiteral(resourceName: "sideAddListining"),#imageLiteral(resourceName: "sideAddListining")]),section(sectionName: "User", sectionOjbect: ["My Listing","Profile","Notifications","Your Quote Data","Logout"], sectionImages: [#imageLiteral(resourceName: "sideListingIcon"),#imageLiteral(resourceName: "sideProfileIcon"),#imageLiteral(resourceName: "sideNotification"),#imageLiteral(resourceName: "sideQuoteIcon"),#imageLiteral(resourceName: "sideLogoutIcon")]),section(sectionName: "Communication", sectionOjbect: ["Contact","Terms and Conditions","Rate This App","Share App"] ,sectionImages: [#imageLiteral(resourceName: "sideContactIcon"),#imageLiteral(resourceName: "sideTermsIcon"),#imageLiteral(resourceName: "sideStareIcon"),#imageLiteral(resourceName: "sideShareIcon")])]
            
            self.userinfoview.isHidden = false
            self.useremail.text = UserDefaults.standard.string(forKey: "user_login")
            self.username.text = UserDefaults.standard.string(forKey: "username")
            var pic = UserDefaults.standard.string(forKey: "profilepic")
            
            if pic == ""
            {
            
            }
            else if pic != ""
            {
                pic = UserDefaults.standard.string(forKey: "profilepic")
                self.profileimage.downloadedFrom(link:pic ?? "", view: self.view)
            }
            
            
            
            
            self.profileimage?.layer.masksToBounds = false
            
            profileimage?.layer.cornerRadius = (profileimage?.frame.height)!/2
            profileimage?.clipsToBounds = true
            
        }
        else {
            objectSection = [section(sectionName: "", sectionOjbect: ["Home","Request a Quote","Categories","About US","Add Listning","Afflitate Area"], sectionImages: [#imageLiteral(resourceName: "sideHome"),#imageLiteral(resourceName: "sideRequest"),#imageLiteral(resourceName: "sideAboutUs"),#imageLiteral(resourceName: "sideAboutUs"),#imageLiteral(resourceName: "sideAddListining"),#imageLiteral(resourceName: "sideAddListining")]),section(sectionName: "User", sectionOjbect: ["Join Now"], sectionImages: [#imageLiteral(resourceName: "sideJoinNow")]),section(sectionName: "Communication", sectionOjbect: ["Contact","Terms and Conditions","Rate This App","Share App"] ,sectionImages: [#imageLiteral(resourceName: "sideContactIcon"),#imageLiteral(resourceName: "sideTermsIcon"),#imageLiteral(resourceName: "sideStareIcon"),#imageLiteral(resourceName: "sideShareIcon")])]
            self.userinfoview.isHidden = true
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        objectHome.hideMenuFunc()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return objectSection.count 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectSection[section].sectionOjbect.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuTableViewCell", for: indexPath) as! sideMenuTableViewCell
        cell.sideLabelOutlet.text = objectSection[indexPath.section].sectionOjbect[indexPath.row]
        cell.sideImageOutlet.image = images[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Request a Quote" {
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "requestQuoteView") as! requestQuoteView
            self.present(VC, animated: true, completion: nil)

        }
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "About US"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "aboutUsView") as! aboutUsView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Join Now"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "signInView") as! signInView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Contact"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "ContactUSView") as! ContactUSView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Terms and Conditions"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "termAndConditionView") as! termAndConditionView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Categories"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "categoryViewController") as! categoryViewController
            self.present(VC, animated: true, completion: nil)
        }
            
            //MyNotifications
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Notifications"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "MyNotifications") as! MyNotifications
            self.present(VC, animated: true, completion: nil)
        }
            
            
            
            
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Logout"{
            AppDelegate.menuBool = true
            let alert = UIAlertController(title: "Alert", message: "Do You Really want to Logut?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes" , style: UIAlertActionStyle.default, handler:{ action in
                UserDefaults.standard.set(false, forKey: "isLogin")
                self.VC = self.story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
                self.present(self.VC, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "No" , style: UIAlertActionStyle.default, handler:{ action in
                
                self.VC = self.story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
                self.present(self.VC, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Home"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Profile"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "updateProfileView") as! updateProfileView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Your Quote Data"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "quoteDataView") as! quoteDataView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "My Listing"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "myLIstingView") as! myLIstingView
            self.present(VC, animated: true, completion: nil)
        }
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Afflitate Area"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "affliateAreaView") as! affliateAreaView
            self.present(VC, animated: true, completion: nil)
        }
            
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Add Listning"{
            AppDelegate.menuBool = true
            VC = story.instantiateViewController(withIdentifier: "listingview") as! ListingView
            self.present(VC, animated: true, completion: nil)
        }
        
        
        else if objectSection[indexPath.section].sectionOjbect[indexPath.row] == "Share App"{
           
                let urlString = "https://www.google.com"
            
            
            
                let linkToShare = [urlString]
            
                let activityController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
            
                self.present(activityController, animated: true, completion: nil)
            
            
        }
        
    }
    
    

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return objectSection[section].sectionName
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        
        //view.tintColor = UIColor.red
        let header = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor = UIColor.white
        header.textLabel?.textColor = #colorLiteral(red: 0.3685981035, green: 0.3686446249, blue: 0.3685767055, alpha: 1)
        header.textLabel?.font = header.textLabel?.font.withSize(15)
        
    }
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

  

} // end of sideMenuView Class
