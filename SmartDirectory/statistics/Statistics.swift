//
//  Statistics.swift
//  SmartDirectory
//
//  Created by mac  on 19/10/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import SwiftyJSON

class Statistics: UIViewController {

    @IBOutlet weak var referralslbl: UILabel!
    @IBOutlet weak var visitsllbl: UILabel!
    @IBOutlet weak var unpaidearninglbl: UILabel!
    @IBOutlet weak var paidearninglbl: UILabel!
    @IBOutlet weak var commisionlbl: UILabel!
    
    var dataFromServer = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.apiCallFunc()
    }
    
    
    
    @IBAction func backbtn(_ sender: Any) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "affliateAreaView") as! affliateAreaView
        self.present(VC, animated: true, completion: nil)
    }
    
    func apiCallFunc() {
        
        let userid = UserDefaults.standard.string(forKey: "userID")
        print("userid is \(userid!)")
        
        let url = URL(string: "https://www.acbd.com.au/iOS-api/getAffiliates.php?")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "userID=\(userid!)"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Check for error
            if error != nil {
                print("error=\(String(describing: error))")
                //  self.databaseError = error
                return
            }
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            let json = JSON(parseJSON: responseString! as String)
            print("json value \(json)")
            var rate = json[0]["rate"].stringValue
            var paidearn = json[0]["earnings"].stringValue
            var unpaid = json[0]["unpaid_earnings"].stringValue
            var referls = json[0]["referrals"].stringValue
            var visits = json[0]["visits"].stringValue
            
            
            if rate.isEmpty == true{
                rate = "0"
            }
            if paidearn.isEmpty == true
            {
                paidearn = "0"
            }
            if unpaid.isEmpty == true
            {
                unpaid = "0"
            }
            if referls.isEmpty == true{
                referls = "0"
            }
            if visits.isEmpty == true
            {
                visits = "0"
            }
            self.commisionlbl.text = rate
            self.paidearninglbl.text = paidearn
            self.unpaidearninglbl.text = unpaid
            self.referralslbl.text = referls
            self.visitsllbl.text = visits
        }
        task.resume()
    }
    

    
    func filter() {
        if dataFromServer.isEmpty == true {
            showAlert(message: "Some thing Wrong!")
        }
        else {
            for (key,value) in dataFromServer {
                if key == "msg" {
                    showAlert(message: value as! String)
                }
            }
        }
        
    }
    
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
    

