//
//  signUpView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 18/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class signUpView: UIViewController,UITextViewDelegate,UITextFieldDelegate {

    @IBOutlet var backgroundViewOutlet: UIView!
    @IBOutlet weak var userNameTextFieldOutlet: UITextField!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    @IBOutlet weak var middleViewOutlet: UIView!
    @IBOutlet weak var middleEmailOutlet: UITextField!
    @IBOutlet weak var middleGetPassOutlet: UIButton!
    @IBOutlet weak var middleCancelOutlet: UIButton!
    
    var dataFromServer = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameTextFieldOutlet.layer.borderWidth = 2
        userNameTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        userNameTextFieldOutlet.layer.cornerRadius = 6
        userNameTextFieldOutlet.delegate = self
        userNameTextFieldOutlet.setLeftPaddingPoints(15)
        
        emailTextFieldOutlet.layer.borderWidth = 2
        emailTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        emailTextFieldOutlet.layer.cornerRadius = 6
        emailTextFieldOutlet.delegate = self
        
        middleEmailOutlet.layer.borderWidth = 2
        middleEmailOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        middleEmailOutlet.layer.cornerRadius = 6
        middleEmailOutlet.delegate = self
        middleEmailOutlet.setLeftPaddingPoints(15)
        
        signUpButtonOutlet.layer.cornerRadius = 8
        emailTextFieldOutlet.setLeftPaddingPoints(15)
        middleViewOutlet.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(signInView.tapGestureToBackGrounViewFunc))
        backgroundViewOutlet.isUserInteractionEnabled = true
        backgroundViewOutlet.addGestureRecognizer(tapGesture)
        
    } // end of viewDidLoad
    
    @objc func tapGestureToBackGrounViewFunc(sender:UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userNameTextFieldOutlet.resignFirstResponder()
        emailTextFieldOutlet.resignFirstResponder()
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func forgetPassFunc(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.9, animations: {
            self.middleViewOutlet.layer.borderWidth = 0.5
            self.middleViewOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        }, completion: {
            finished in
            self.middleViewOutlet.isHidden = false
        })
    }
    
    @IBAction func middleCancelFunc(_ sender: UIButton) {
        middleViewOutlet.isHidden = true
    }

    @IBAction func signUpFunc(_ sender: UIButton) {
        
        let url = URL(string: "https://acbd.com.au/iOS-api/UserRegister.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "email=\(userNameTextFieldOutlet.text!)&username=\(emailTextFieldOutlet.text!)"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Check for error
            if error != nil {
                print("error=\(String(describing: error))")
                //  self.databaseError = error
                return
            }
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            do {
                //saving json data from response string url as nsdictionary
                self.dataFromServer = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary as! [String : Any]
                self.filter()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        task.resume()
    } // end of class
    
    func filter() {
        if dataFromServer.isEmpty == true {
            showAlert(message: "Some thing Wrong!")
        }
        else {
            for (key,value) in dataFromServer {
                if key == "msg" {
                    showAlert(message: value as! String)
                }
            }
        }
        
    } // end
    
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}// end of class
