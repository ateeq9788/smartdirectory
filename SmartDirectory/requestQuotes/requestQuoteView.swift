//
//  requestQuoteView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 17/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class requestQuoteView: UIViewController,UITextFieldDelegate,UITextViewDelegate {

    @IBOutlet weak var needTextFieldOutlet: UITextField!
    @IBOutlet weak var subrubTextFieldOutlet: UITextField!
    @IBOutlet weak var startTextFieldOutlet: UITextField!
    @IBOutlet weak var describeTextViewOutlet: UITextView!
    @IBOutlet weak var nameTextViewOutlet: UITextField!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    @IBOutlet weak var contactTextFieldOutlet: UITextField!
    @IBOutlet weak var addressTextViewOutlet: UITextView!
    @IBOutlet weak var getQuoteButtonOutlet: UIButton!
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    @IBOutlet weak var backGroundViewOutlet: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var parameters = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.color = UIColor.white
        indicator.isHidden = true
        indicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        
        needTextFieldOutlet.layer.borderWidth = 2
        needTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        needTextFieldOutlet.layer.cornerRadius = 6
        needTextFieldOutlet.delegate = self
        needTextFieldOutlet.setLeftPaddingPoints(15)
        
        subrubTextFieldOutlet.layer.borderWidth = 2
        subrubTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        subrubTextFieldOutlet.layer.cornerRadius = 6
        subrubTextFieldOutlet.delegate = self
        subrubTextFieldOutlet.setLeftPaddingPoints(15)
        
        startTextFieldOutlet.layer.borderWidth = 2
        startTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        startTextFieldOutlet.layer.cornerRadius = 6
        startTextFieldOutlet.delegate = self
        startTextFieldOutlet.setLeftPaddingPoints(15)
        
        describeTextViewOutlet.layer.borderWidth = 2
        describeTextViewOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        describeTextViewOutlet.layer.cornerRadius = 6
        describeTextViewOutlet.delegate = self
        
        nameTextViewOutlet.layer.borderWidth = 2
        nameTextViewOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        nameTextViewOutlet.layer.cornerRadius = 6
        nameTextViewOutlet.delegate = self
        nameTextViewOutlet.setLeftPaddingPoints(15)
        
        emailTextFieldOutlet.layer.borderWidth = 2
        emailTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        emailTextFieldOutlet.layer.cornerRadius = 6
        emailTextFieldOutlet.delegate = self
        emailTextFieldOutlet.setLeftPaddingPoints(15)
        
        contactTextFieldOutlet.layer.borderWidth = 2
        contactTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        contactTextFieldOutlet.layer.cornerRadius = 6
        contactTextFieldOutlet.delegate = self
        contactTextFieldOutlet.setLeftPaddingPoints(15)
        
        addressTextViewOutlet.layer.borderWidth = 2
        addressTextViewOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        addressTextViewOutlet.layer.cornerRadius = 6
        addressTextViewOutlet.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(signInView.tapGestureToBackGrounViewFunc))
        backGroundViewOutlet.isUserInteractionEnabled = true
        backGroundViewOutlet.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func tapGestureToBackGrounViewFunc(sender:UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        needTextFieldOutlet.resignFirstResponder()
        subrubTextFieldOutlet.resignFirstResponder()
        startTextFieldOutlet.resignFirstResponder()
        nameTextViewOutlet.resignFirstResponder()
        emailTextFieldOutlet.resignFirstResponder()
        contactTextFieldOutlet.resignFirstResponder()
        
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.tag == 2 {
            scrollViewOutlet.setContentOffset(CGPoint(x:0, y:800), animated: true)
        }
        
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView.tag == 2 {
            scrollViewOutlet.setContentOffset(CGPoint(x:0, y:0), animated: true)
        }
        return true
    }
    
    
    
    
    
    
    @IBAction func postQuotesFunc(_ sender: UIButton) {
        self.indicator.isHidden = false
        self.indicator.startAnimating()
        parameters = ["need:\(needTextFieldOutlet.text!), subrub:\(subrubTextFieldOutlet.text!), start:\(startTextFieldOutlet.text!), describe:\(describeTextViewOutlet.text!), name:\(nameTextViewOutlet.text!), email:\(emailTextFieldOutlet.text!) , contact:\(contactTextFieldOutlet.text!), address: \(addressTextViewOutlet.text!)"]
        print(parameters)
        
//        DispatchQueue.global(qos: .background).async {
//            self.sendQuoteFunc()
//        }
        
       // DispatchQueue.main.async{
            self.sendqout()
        //}
        
    }
    
    
    func sendqout()
    {
       
        
        let parameters: Parameters =  ["need":"\(needTextFieldOutlet.text!)", "subrub":"\(subrubTextFieldOutlet.text!)", "start":"\(startTextFieldOutlet.text!)", "describe":"\(describeTextViewOutlet.text!)", "name":"\(nameTextViewOutlet.text!)", "email":"\(emailTextFieldOutlet.text!)" , "contact":"\(contactTextFieldOutlet.text!)", "address": "\(addressTextViewOutlet.text!)"]
        
        
        
        
        Alamofire.request("https://acbd.com.au/iOS-api/send_email.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()
            
            let json = try? JSON(data: response.data!)
            
            print("json data \(json!)")
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            if json!["success"] == 1
            {
                print("1")
                self.showAlert(message:"Your email has been sent successfully")
            }
            else
            {
                self.showAlert(message:"Error occured Try Again!")
            }
    }
    }
    
//    func sendQuoteFunc() {
//
//        guard let url = URL(string: "https://acbd.com.au/iOS-api/insertQuote.php") else {return}
//        var request = URLRequest(url: url)
//        guard let httpBody = try?  JSONSerialization.data(withJSONObject: parameters, options: []) else { return}
//        request.httpBody = httpBody
//        request.httpMethod = "POST"
//
//        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
//            guard let dataResponse = data,
//                error == nil else {
//                    print(error?.localizedDescription ?? "Response Error")
//                    return }
//
//            let dataString = String(data: dataResponse, encoding: .utf8) ?? ""
//            print(dataString)
//            self.message = dataString
//
//            if dataString == "Successfully" {
//                self.showAlert(message: "Quote submit Successfully")
//                DispatchQueue.main.async {
//                    self.indicator.stopAnimating()
//                    self.indicator.isHidden = true
//                }
//
//            }
//            else {
//                self.showAlert(message: "Error Occured, Try Again!")
//                DispatchQueue.main.async {
//                    self.indicator.stopAnimating()
//                    self.indicator.isHidden = true
//                }
//
//            }
//
//        }
//
//
//        task.resume()
//
//
//
//    }
    
    
    @IBAction func backFunc(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)

    }
    
    func showAlert(message:String) {
        
        
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
      
    
       
    }
    
} // end fo requestQuoteView class
