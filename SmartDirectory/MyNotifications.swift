//
//  MyNotifications.swift
//  SmartDirectory
//
//  Created by mac  on 12/11/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class MyNotifications: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonFunc(_ sender: UIButton) {
        
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
        
    }

}
