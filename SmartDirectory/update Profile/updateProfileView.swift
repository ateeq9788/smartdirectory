//
//  updateProfileView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 08/10/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit

class updateProfileView: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate , UIScrollViewDelegate {

    @IBOutlet weak var profilePictureOutlet: UIImageView!
    @IBOutlet weak var firstNameTextFieldOutlet: UITextField!
    @IBOutlet weak var lastNameTextFieldOutlet: UITextField!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    @IBOutlet weak var phoneTextFieldOutlet: UITextField!
    @IBOutlet weak var addressTextViewOutlet: UITextView!
    @IBOutlet weak var aboutTextViewOutlet: UITextView!
    @IBOutlet weak var facebookTextFieldOutlet: UITextField!
    @IBOutlet weak var twitterTextFieldOutlet: UITextField!
    @IBOutlet weak var linkedInTextFieldOutlet: UITextField!
    @IBOutlet weak var googleTextFieldOutlet: UITextField!
    @IBOutlet weak var instagramTextFieldOutlet: UITextField!
    @IBOutlet weak var pinterestTextFieldOutlet: UITextField!
    @IBOutlet weak var newPasswordTextFieldOutlet: UITextField!
    @IBOutlet weak var repeatPasswordTextFieldOutlet: UITextField!
    
    @IBOutlet var scrollview: UIScrollView!
    
    @IBOutlet weak var navigationview: UIView!
    
    
    
    
    
    
    var dataFromServer = [[String:Any]]()
    var dataFromServerUpdate = [[String:Any]]()
    
    var imageToConvert = UIImage()
    var picture = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollview.delegate = self
        
        
        self.scrollview.frame = CGRect(x:0,y:self.navigationview.frame.size.height + 5,width: self.view.frame.width ,height:self.scrollview.frame.height)
        
        
        self.scrollview.contentSize = CGSize(width: scrollview.contentSize.width - 5, height:(self.scrollview.frame.height)+(self.scrollview.frame.height - self.view.frame.height) + 50)
        
        self.view.addSubview(self.scrollview)
        
        
        addressTextViewOutlet.layer.borderWidth = 1
        addressTextViewOutlet.layer.borderColor = UIColor.black.cgColor
        
        aboutTextViewOutlet.layer.borderWidth = 1
        aboutTextViewOutlet.layer.borderColor = UIColor.black.cgColor
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        firstNameTextFieldOutlet.resignFirstResponder()
        lastNameTextFieldOutlet.resignFirstResponder()
        emailTextFieldOutlet.resignFirstResponder()
        phoneTextFieldOutlet.resignFirstResponder()
        facebookTextFieldOutlet.resignFirstResponder()
        twitterTextFieldOutlet.resignFirstResponder()
        linkedInTextFieldOutlet.resignFirstResponder()
        googleTextFieldOutlet.resignFirstResponder()
        instagramTextFieldOutlet.resignFirstResponder()
        pinterestTextFieldOutlet.resignFirstResponder()
        newPasswordTextFieldOutlet.resignFirstResponder()
        repeatPasswordTextFieldOutlet.resignFirstResponder()
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let id = UserDefaults.standard.integer(forKey: "userID")
        let paramter = "user_id=\(id)"
        DispatchQueue.global(qos: .background).async {
            self.apiCallFunc(postString: paramter)
        }
    }
    
    
    func apiCallFunc(postString : String) {
        
        let url = URL(string: "https://acbd.com.au/iOS-api/getUserDetails.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Check for error
            if error != nil {
                print("error=\(String(describing: error))")
                //  self.databaseError = error
                return
            }
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            do {
                //saving json data from response string url as nsdictionary
                self.dataFromServer = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray as! [[String : Any]]
                self.filterData()
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        task.resume()
    } // end
    
    func filterData () {
        var userData : userDetail!
        for data in dataFromServer {
            userData = userDetail(data)
        }
        
        DispatchQueue.main.async {
            
            self.profilePictureOutlet.sd_setShowActivityIndicatorView(true)
            self.profilePictureOutlet.sd_setIndicatorStyle(.gray)
            self.profilePictureOutlet.sd_setImage(with: URL(string:userData.pic), placeholderImage: #imageLiteral(resourceName: "imageLoaderIcon") , options: [.highPriority], completed: nil)
            self.firstNameTextFieldOutlet.text = userData.firstName
            self.lastNameTextFieldOutlet.text = userData.lastName
            self.emailTextFieldOutlet.text = userData.email
            self.phoneTextFieldOutlet.text = userData.phone
            self.addressTextViewOutlet.text = userData.address
            self.aboutTextViewOutlet.text = userData.about
            self.facebookTextFieldOutlet.text = userData.facebook
            self.twitterTextFieldOutlet.text = userData.twitter
            self.linkedInTextFieldOutlet.text = userData.linkedin
            self.googleTextFieldOutlet.text = userData.google
            self.instagramTextFieldOutlet.text = userData.instgram
            self.pinterestTextFieldOutlet.text = userData.pinterest
            
            
        }
    }

    
    @IBAction func uploadProfilePictureFunc(_ sender: UIButton) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    @IBAction func uploadProfileFunc(_ sender: UIButton) {
        let id = UserDefaults.standard.integer(forKey: "userID")
        let paramters = "user_id=\(id)&first_name=\(firstNameTextFieldOutlet.text!)&last_name=\(lastNameTextFieldOutlet.text!)&email=\(emailTextFieldOutlet.text!)&phone=\(phoneTextFieldOutlet.text!)&address=\(addressTextViewOutlet.text)&description=\(aboutTextViewOutlet.text!)&facebook=\(facebookTextFieldOutlet.text!)google=\(googleTextFieldOutlet.text!)linkedin=\(linkedInTextFieldOutlet.text!)instagram=\(instagramTextFieldOutlet.text!)twitter=\(twitterTextFieldOutlet.text!)pinterest=\(pinterestTextFieldOutlet.text!)password=\(newPasswordTextFieldOutlet.text!)confirm_password=\(repeatPasswordTextFieldOutlet.text!)"
      apiCalltoUpdate(postString: paramters)
        
        
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if info[UIImagePickerControllerOriginalImage] != nil{
            
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMddHHmmss"
            let dateString = dateFormatter.string(from:date)
            
            imageToConvert = (info[UIImagePickerControllerOriginalImage] as! UIImage)
            profilePictureOutlet.image = (info[UIImagePickerControllerOriginalImage] as! UIImage)
            profilePictureOutlet.layer.cornerRadius = profilePictureOutlet.frame.size.width / 2
            profilePictureOutlet.layer.masksToBounds = false
            profilePictureOutlet.clipsToBounds = true
            profilePictureOutlet.layer.borderWidth = 1
            profilePictureOutlet.layer.borderColor = UIColor.black.cgColor
            
            let imgData = UIImageJPEGRepresentation(imageToConvert, 0.5)!
           /* Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "fileset",fileName: "IMG_\(dateString).jpg", mimeType: "image/jpg")
            }, to:"http://qem.vsol.pk/upload.php")
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                        self.picture = "http://qem.vsol.pk/uploads/IMG_\(dateString).jpg"
                        //let c = Int((progress.fractionCompleted) * 100)
                        //self.UploadprogressLabelOutlet.text = String("\(c) %")
                        self.progressBarOutlet.observedProgress = progress
                        
                    })
                    upload.responseJSON { response in
                        print(response.result.value as Any)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
            }*/
            
        }
        self.dismiss(animated: true, completion: nil)
    } // end of didFinishPickingMediaWithInfo

    
    @IBAction func backButtonFunc(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
        
    }
    
    func apiCalltoUpdate(postString : String) {
        
        let url = URL(string: "https://acbd.com.au/iOS-api/updateUserProfile.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Check for error
            if error != nil {
                print("error=\(String(describing: error))")
                //  self.databaseError = error
                return
            }
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            self.showAlert(message: responseString! as String)
        }
        task.resume()
    } // end

   
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

   

} // end of class
