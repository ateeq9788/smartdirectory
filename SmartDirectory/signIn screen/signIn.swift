//
//  signInView.swift
//  SmartDirectory
//
//  Created by Adnan Yousaf on 16/09/2018.
//  Copyright © 2018 Adnan Yousaf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class signInView: UIViewController,UITextFieldDelegate {

    @IBOutlet var backgroundViewOutlet: UIView!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    @IBOutlet weak var passwordTextFieldOutlet: UITextField!
    @IBOutlet weak var signInButtonOutlet: UIButton!
    @IBOutlet weak var middleViewOutlet: UIView!
    @IBOutlet weak var middleEmailOutlet: UITextField!
    @IBOutlet weak var middleGetPassOutlet: UIButton!
    @IBOutlet weak var middleCancelOutlet: UIButton!
    
    var dataFromServer = [[String:Any]]()
    var dataFromServerPassword = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()

        emailTextFieldOutlet.layer.borderWidth = 2
        emailTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        emailTextFieldOutlet.layer.cornerRadius = 6
        emailTextFieldOutlet.delegate = self
        emailTextFieldOutlet.setLeftPaddingPoints(15)
        
        passwordTextFieldOutlet.layer.borderWidth = 2
        passwordTextFieldOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        passwordTextFieldOutlet.layer.cornerRadius = 6
        passwordTextFieldOutlet.delegate = self
        
        middleEmailOutlet.layer.borderWidth = 2
        middleEmailOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        middleEmailOutlet.layer.cornerRadius = 6
        middleEmailOutlet.delegate = self
        middleEmailOutlet.setLeftPaddingPoints(15)
        
        signInButtonOutlet.layer.cornerRadius = 8
        passwordTextFieldOutlet.setLeftPaddingPoints(15)
        middleViewOutlet.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(signInView.tapGestureToBackGrounViewFunc))
        backgroundViewOutlet.isUserInteractionEnabled = true
        backgroundViewOutlet.addGestureRecognizer(tapGesture)
        
    } // end of viewDidLoad
    
    @objc func tapGestureToBackGrounViewFunc(sender:UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailTextFieldOutlet.resignFirstResponder()
        passwordTextFieldOutlet.resignFirstResponder()
        return true
    }
    
    @IBAction func forgetPassFunc(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.9, animations: {
            self.middleViewOutlet.layer.borderWidth = 0.5
            self.middleViewOutlet.layer.borderColor = #colorLiteral(red: 0.8705241084, green: 0.8706257939, blue: 0.8704773188, alpha: 1)
        }, completion: {
            finished in
            self.middleViewOutlet.isHidden = false
        })
    }
    
    @IBAction func middleCancelFunc(_ sender: UIButton) {
        middleViewOutlet.isHidden = true
    }
    
    @IBAction func backFunc(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle : nil)
        let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
        self.present(VC, animated: true, completion: nil)
    }
    
    
    @IBAction func signInFunc(_ sender: UIButton) {

    
    
    self.login()
        
    } // end of class
    

    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func login(){
        

        
        Alamofire.request("https://acbd.com.au/iOS-api/getLogin.php?username=\(emailTextFieldOutlet.text!)&password=\(passwordTextFieldOutlet.text!)", method: .post, parameters: nil, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()
            
            let json = try? JSON(data: response.data!)
            
            print("json data \(json!)")
            
            if json!.isEmpty
            {
                self.showAlert(message: "User Not Found!")
            }
            else
            {
                let userid = "\(json![0]["ID"])"
                
                UserDefaults.standard.set(userid, forKey: "userID")
                
                let mail = json![0]["user_login"].stringValue
                 UserDefaults.standard.set(mail, forKey: "user_login")
                
                self.getUserDetail()
            }
            
        
        }
    }
    
    
    func getUserDetail(){
        
        let userid = UserDefaults.standard.string(forKey: "userID")
        
//        let parameters: Parameters = [
//            "user_id": userid!,
//            ]
        
        
        Alamofire.request("https://acbd.com.au/iOS-api/getUserDetails.php?user_id=\(userid!)", method: .post, parameters: nil, encoding: JSONEncoding.default).validate(contentType:["application/json"]).responseJSON { response in
            debugPrint(response)
            // self.activiity.stopAnimating()
            
            let json = try? JSON(data: response.data!)
            
            print("get user detail data \(json!)")
            
            if json!.isEmpty{
                
            }
        
            else{
            //listingpro_author_img_url
            
            let username =  json![0]["user_nicename"].stringValue
            print("user name is \(username)")
            
            
            UserDefaults.standard.set(username, forKey: "username")
            
            let userdata = json![0]["userDeatils"]
            
            for i in (0..<userdata.count)
            {
                if (userdata[i][["meta_key"]].stringValue == "listingpro_author_img_url")
                {
                            print("user profile pic value \(userdata[i][["meta_value"]].stringValue)")
                    let profilepic = userdata[i][["meta_value"]].stringValue
                    
                    UserDefaults.standard.set(profilepic, forKey: "profilepic")
                    
                }
                
                else if (userdata[i][["meta_key"]].stringValue == "facebook")
                {
                    let facebook = userdata[i][["meta_value"]].stringValue
                    
                    UserDefaults.standard.set(facebook, forKey: "facebook")
                    
                }
                
                else if (userdata[i][["meta_key"]].stringValue == "google")
                {
                    let google = userdata[i][["meta_value"]].stringValue
                    
                    UserDefaults.standard.set(google, forKey: "google")
                    
                }
                
             
                else if (userdata[i][["meta_key"]].stringValue == "linkedin")
                {
                    let linkedin = userdata[i][["meta_value"]].stringValue
                    
                    UserDefaults.standard.set(linkedin, forKey: "linkedin")
                    
                }
                
                
                else if (userdata[i][["meta_key"]].stringValue == "instagram")
                {
                    let instagram = userdata[i][["meta_value"]].stringValue
                    
                    UserDefaults.standard.set(instagram, forKey: "instagram")
                    
                }
                
                else if (userdata[i][["meta_key"]].stringValue == "twitter")
                {
                    let twitter = userdata[i][["meta_value"]].stringValue
                    
                    UserDefaults.standard.set(twitter, forKey: "twitter")
                    
                }
                
                else if (userdata[i][["meta_key"]].stringValue == "pinterest")
                {
                    let pinterest = userdata[i][["meta_value"]].stringValue
                    
                    UserDefaults.standard.set(pinterest, forKey: "pinterest")
                    
                }
                
            }
                
                UserDefaults.standard.set(true, forKey: "isLogin")
                let story = UIStoryboard(name: "Main", bundle : nil)
                let VC = story.instantiateViewController(withIdentifier: "homeScreenView") as! homeScreenView
                self.present(VC, animated: true, completion: nil)
                
                
            }
    }
    
    }
    
    @IBAction func getPasswordOutlet(_ sender: UIButton) {
       /* let url = URL(string: "https://acbd.com.au/iOS-api/forgotPassword.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "email=\(emailTextFieldOutlet.text!)"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Check for error
            if error != nil {
                print("error=\(String(describing: error))")
                //  self.databaseError = error
                return
            }
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            do {
                //saving json data from response string url as nsdictionary
                self.dataFromServerPassword = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray as! [[String : Any]]
                print(self.dataFromServerPassword)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        task.resume()*/
    }
    
    
    

} // end of signIn class
